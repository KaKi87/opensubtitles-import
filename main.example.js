import doImport from './mod.js';

await doImport({
    rootPath: undefined,
    tsvPath: undefined,
    tsvHeaderLineIndex: undefined,
    tsvFirstItemLineIndex: undefined,
    getSrtPath: undefined,
    onItem: async ({
        item,
        srtContent
    }) => {}
});