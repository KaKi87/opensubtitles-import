import {
    readLines
} from 'https://deno.land/std@0.161.0/io/mod.ts';
import {
    join as joinPath
} from 'https://deno.land/std@0.161.0/path/mod.ts';

export default async ({
    rootPath = 'data/export_eng_all_data',
    tsvPath = 'export.txt',
    tsvHeaderLineIndex = 2,
    tsvFirstItemLineIndex = 3,
    getSrtPath = item => joinPath(rootPath, ...item['IDSubtitleFile'].split('').reverse().slice(0, 4), item['IDSubtitleFile']),
    onItem
}) => {
    const tsvFile = await Deno.open(joinPath(rootPath, tsvPath));

    {
        let lineIndex = 0;
        const keys = [];
        for await (const line of readLines(tsvFile)){
            if(lineIndex === tsvHeaderLineIndex){
                keys.push(...line.split('\t'));
            }
            if(lineIndex >= tsvFirstItemLineIndex){
                const item = Object.fromEntries(line.split('\t').map((value, index) => [keys[index], value]));
                await onItem({
                    item,
                    srtContent: await Deno.readTextFile(getSrtPath(item))
                });
            }
            lineIndex++;
        }
    }
    
    await tsvFile.close();
};